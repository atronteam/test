Map category1 = {
  "id": 1,
  "name": "بستنی",
};
Map category2 = {
  "id": 2,
  "name": "ماست",
};
Map category3 = {
  "id": 3,
  "name": "دوغ",
};
Map category4 = {
  "id": 4,
  "name": "بیسکوئیت",
};
Map category5 = {
  "id": 5,
  "name": "همه دسته بندی ها",
};

List category = [
  category5,
  category1,
  category2,
  category3,
  category4,
];
