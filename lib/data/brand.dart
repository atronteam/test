Map brand1 = {
  "name": "میهن",
  "id": 1,
};

Map brand2 = {
  "name": "عالیس",
  "id": 2,
};

Map brand3 = {
  "name": "لومنت",
  "id": 3,
};

Map brand4 = {
  "name": "گرجی",
  "id": 4,
};

Map brand5 = {
  "name": "تمامی برند ها",
  "id": 5,
};

List brand = [
  brand5,
  brand1,
  brand2,
  brand3,
  brand4,
];
