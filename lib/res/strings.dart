class StringsApp {
  static String single = "تخفیف تکی";
  static String group = "تخفیف گروهی";
  static String allCategory = "همه دسته بندی ها";
  static String category = "دسته بندی";
  static String products = "محصول";
  // static String selectCategory = "دسته بندی";
  static String codeTakhfif = "کد تخفیف";
  static String yogort = "ماست";
  static String iceCreem = "بستنی";
  static String dogh = "دوغ";
  static String brand = "برند";
  static String tedadi = "تعدادی";
  static String percent = "درصدی";
  static String price = "مبلغ ثابت";
  static String accept = "تایید";
  static String typeTakhfif = "نوع تخفیف";
  static String lowPrice = "حداقل قیمت";
  static String lowCount = "حداقل تعداد";
  static String countTakhfif = "تعداد تخفیف";
  static String lowFactor = "حداقل مبلغ فاکتور";
  static String priceTakhfif = "مبلغ تخفیف";
  static String selectBrand = "برند خود را انتخاب کنید";
}
