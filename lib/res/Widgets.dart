import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:testApp/pages/Customer/SingleProduct.dart';
import 'package:testApp/res/Assets.dart';
import 'package:testApp/res/Fonts.dart';
import 'package:testApp/res/colors.dart';

class ProductCards extends StatefulWidget {
  final id;
  final image;
  final name;
  final price;
  final description;
  final category;
  final count;
  final offer;

  const ProductCards(
      {this.id,
      this.image,
      this.name,
      this.price,
      this.description,
      this.category,
      this.count,
      this.offer});

  @override
  _ProductCardsState createState() => _ProductCardsState(
      id: this.id,
      image: this.image,
      name: this.name,
      price: this.price,
      description: this.description,
      category: this.category,
      count: this.count,
      offer: this.offer
  );
}

class _ProductCardsState extends State<ProductCards> {
  final id;
  final image;
  final name;
  final price;
  final description;
  final category;
  final count;
  final offer;

  _ProductCardsState(
      {this.description,
      this.category,
      this.count,
      this.offer,
      this.id,
      this.image,
      this.name,
      this.price});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(
                builder:(BuildContext context) => SingleProduct(
                  id: this.id,
                  image: this.image,
                  name: this.name,
                  price: this.price,
                  description: this.description,
                  category: this.category,
                  count: this.count,
                  offer: this.offer
                )));
          },
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Container(
              width: 230,
              height: height / 3,
              child: Stack(
                fit: StackFit.loose,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      width: 180,
                      height: height / 4,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                              spreadRadius: 2,
                              blurRadius: 5.0,
                            )
                          ],
                          image: DecorationImage(
                              image: AssetImage(this.image),
                              fit: BoxFit.cover,
                              alignment: Alignment.center)),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          "${this.price.toString() + " تومان "}",
                          textDirection: TextDirection.rtl,
                          style: TextStyle(
                            fontFamily: FontsApp.iranYekan,
                            color: ColorsApp.button,
                          ),
                        ),
                        Text(
                          this.name,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: FontsApp.iranYekan,
                            color: ColorsApp.text,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
    );
  }
}
