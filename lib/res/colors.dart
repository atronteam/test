import 'package:flutter/material.dart';

class ColorsApp {
  static Color button = Color(0xff4f48ec);
  static Color card = Color(0xffc8c7dd);
  static Color text = Color(0xff100e34);
  static Color background = Colors.white;
}
