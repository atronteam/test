import 'dart:convert';

class Product {
  
    Product({
        this.id,
        this.price,
        this.name,
        this.description,
        this.image,
        this.category,
        this.count,
        this.offer,
    });

    final int id;
    final int price;
    final String name;
    final String description;
    final String image;
    final int category;
    final int count;
    final int offer;

    factory Product.fromRawJson(String str) => Product.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"],
        price: json["price"],
        name: json["name"],
        description: json["description"],
        image: json["image"],
        category: json["category"],
        count: json["count"],
        offer: json["offer"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "price": price,
        "name": name,
        "description": description,
        "image": image,
        "category": category,
        "count": count,
        "offer": offer,
    };
}
