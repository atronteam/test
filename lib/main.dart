import 'package:flutter/material.dart';
import 'package:testApp/bottomNavigationBar.dart';
import 'package:testApp/res/Fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Test App',
      theme: ThemeData(
          primaryColor: Colors.blueGrey,
          accentColor: Colors.blue,
          textSelectionColor: Colors.black54,
          fontFamily: FontsApp.iranYekan
      ),
      home: BottomNavigation(),
    );
  }
}

