import 'package:flutter/cupertino.dart';

class TypeTakhfifProvider with ChangeNotifier {
  int _lowCount;
  int _lowtakhfif;
  int _lowFactor;
  int _priceTakhfif;
  int _lowPrice;

  TypeTakhfifProvider(this._lowCount, this._lowtakhfif, this._lowFactor,
      this._priceTakhfif, this._lowPrice);

  // getter
  int get lowCount => _lowCount;
  int get lowtakhfif => _lowtakhfif;
  int get lowFactor => _lowFactor;
  int get priceTakhfif => _priceTakhfif;
  int get lowPrice => _lowPrice;

  // setter
  set lowCount(int lowCount) {
    _lowCount = lowCount;
  }

  set lowtakhfif(int lowtakhfif) {
    _lowtakhfif = lowtakhfif;
  }

  set lowFactor(int lowFactor) {
    _lowFactor = lowFactor;
  }

  set priceTakhfif(int priceTakhfif) {
    _priceTakhfif = priceTakhfif;
  }

  set lowPrice(int lowPrice) {
    _lowPrice = lowPrice;
  }
}
