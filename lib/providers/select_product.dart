import 'package:flutter/cupertino.dart';

class SelectProduct with ChangeNotifier {
  List category;
  List brandId;
  SelectProduct(
    this.category,
    this.brandId,
  );
  // getter
  List get getCategory => category;
  List get getBrand => brandId;

  // setter
  set setCategory(List value) {
    category = value;
    notifyListeners();
  }

  set setBrand(List value) {
    brandId = value;
    notifyListeners();
  }
}
