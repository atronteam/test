import 'package:flutter/material.dart';

class PageIndex with ChangeNotifier {
  int _pageindex;
  PageIndex(this._pageindex);
  getPage() => _pageindex;
  setPage(int value) {
    _pageindex = value;
    notifyListeners();
  }
}