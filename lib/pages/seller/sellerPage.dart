import 'package:flutter/material.dart';
import 'package:testApp/pages/seller/group_type.dart';
import 'package:testApp/pages/seller/single_takhfif.dart';
import 'package:testApp/pages/seller/takhfifCode.dart';
import 'package:testApp/res/Fonts.dart';
import 'package:testApp/res/colors.dart';
import 'package:testApp/res/strings.dart';

class SellerPage extends StatefulWidget {
  @override
  _SellerPageState createState() => _SellerPageState();
}

class _SellerPageState extends State<SellerPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          SizedBox(height: 80),
          _groupBtn(),
          SizedBox(height: 20),
          _singleBtn(),
          SizedBox(height: 20),
          _codeBtn(),
        ],
      ),
    );
  }

  _singleBtn() {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SingleTakhfif(),
          ),
        );
      },
      child: Container(
        padding: EdgeInsets.all(10),
        alignment: Alignment.center,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          color: ColorsApp.card,
        ),
        child: Text(
          StringsApp.single,
          style: TextStyle(
              fontFamily: FontsApp.iranSans,
              fontSize: 18,
              color: ColorsApp.text),
        ),
      ),
    );
  }

  _groupBtn() {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (c) => GroupTypeProvider(),
          ),
        );
      },
      child: Container(
        padding: EdgeInsets.all(10),
        alignment: Alignment.center,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          color: ColorsApp.card,
        ),
        child: Text(
          StringsApp.group,
          style: TextStyle(
            fontFamily: FontsApp.iranSans,
            fontSize: 18,
            color: ColorsApp.text,
          ),
        ),
      ),
    );
  }

  _codeBtn() {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => TakhfifCode(),
          ),
        );
      },
      child: Container(
        padding: EdgeInsets.all(10),
        alignment: Alignment.center,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          color: ColorsApp.card,
        ),
        child: Text(
          StringsApp.codeTakhfif,
          style: TextStyle(
              fontFamily: FontsApp.iranSans,
              fontSize: 18,
              color: ColorsApp.text),
        ),
      ),
    );
  }
}
