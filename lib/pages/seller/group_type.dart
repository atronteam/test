import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:testApp/pages/seller/select_brand.dart';
import 'package:testApp/pages/seller/select_category.dart';
import 'package:testApp/pages/seller/select_takhfif.dart';
import 'package:testApp/providers/page_index.dart';
import 'package:testApp/providers/select_product.dart';
import 'package:testApp/res/colors.dart';
import 'package:testApp/res/strings.dart';
import 'package:provider/provider.dart';

class GroupTypeProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => PageIndex(0),
          child: GroupType(),
        ),
        ChangeNotifierProvider(
          create: (context) => SelectProduct(
            [1, 2, 3, 4, 5],
            [1, 2, 3, 4, 5],
          ),
          child: GroupType(),
        ),
      ],
      child: GroupType(),
    );
  }
}

// ignore: must_be_immutable
class GroupType extends StatelessWidget {
  // Divider _divider = Divider(
  //   color: Colors.grey,
  //   indent: 40,
  //   endIndent: 40,
  // );
  // bool init = false;
  // BuildContext _context;
  // SelectProduct _dataProvider;
  // List<Widget> cupertinoListCategory = new List();
  // List<Widget> cupertinoListBrand = new List();

  PageIndex _indexPage;
  @override
  Widget build(BuildContext context) {
    _indexPage = Provider.of<PageIndex>(context);
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 100,
          ),
          _showStatus(),
          SizedBox(
            height: 5,
          ),
          _showStatusSubText(),
          Expanded(
            child: IndexedStack(
              children: [SelectCategory(), SelectBrand(), SelectTakhfif()],
              index: _indexPage.getPage(),
            ),
          ),
        ],
      ),
    );
  }

  Container _showStatusSubText() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            StringsApp.typeTakhfif,
            style: TextStyle(
              color: ColorsApp.text,
              fontSize: 12,
            ),
          ),
          Text(
            StringsApp.brand,
            style: TextStyle(
              color: ColorsApp.text,
              fontSize: 12,
            ),
          ),
          Text(
            StringsApp.category,
            style: TextStyle(
              color: ColorsApp.text,
              fontSize: 12,
            ),
          ),
        ],
      ),
    );
  }

  Container _showStatus() {
    return Container(
      // color: Colors.red,
      height: 24,
      margin: EdgeInsets.symmetric(horizontal: 35),
      child: Stack(
        alignment: Alignment.center,
        children: [
          _divider(),
          _sectionOne(),
          _sectionTow(),
          _sectionThree(),
        ],
      ),
    );
  }

  Divider _divider() {
    return Divider(
      thickness: 1,
      color: Colors.grey,
    );
  }

  Positioned _sectionOne() {
    return Positioned(
      right: 0,
      child: GestureDetector(
        onTap: () {
          if (_indexPage.getPage() > 0) {
            _indexPage.setPage(0);
          }
        },
        child: Container(
          alignment: Alignment.center,
          width: 24,
          height: 24,
          decoration: BoxDecoration(
            border: Border.all(
              color: ColorsApp.button,
            ),
            shape: BoxShape.circle,
            color: ColorsApp.button,
          ),
          child: _indexPage.getPage() > 0
              ? Icon(
                  Icons.edit,
                  color: ColorsApp.background,
                  size: 14,
                )
              : Text(
                  "1",
                  style: TextStyle(
                    color: ColorsApp.background,
                    fontSize: 12,
                  ),
                ),
        ),
      ),
    );
  }

  Positioned _sectionTow() {
    return Positioned(
      left: 1,
      right: 1,
      child: GestureDetector(
        onTap: () {
          if (_indexPage.getPage() > 1) {
            _indexPage.setPage(1);
          }
        },
        child: Container(
          alignment: Alignment.center,
          width: 24,
          height: 24,
          decoration: BoxDecoration(
            border: Border.all(
              color: ColorsApp.button,
            ),
            shape: BoxShape.circle,
            color:
                _indexPage.getPage() >= 1 ? ColorsApp.button : ColorsApp.card,
          ),
          child: _indexPage.getPage() > 1
              ? Icon(
                  Icons.edit,
                  color: ColorsApp.background,
                  size: 14,
                )
              : Text(
                  "2",
                  style: TextStyle(
                    fontSize: 12,
                    color: _indexPage.getPage() >= 1
                        ? ColorsApp.background
                        : ColorsApp.text,
                  ),
                ),
        ),
      ),
    );
  }

  Positioned _sectionThree() {
    return Positioned(
      left: 0,
      child: GestureDetector(
        onTap: () {
          if (_indexPage.getPage() > 2) {
            _indexPage.setPage(2);
          }
        },
        child: Container(
          width: 24,
          height: 24,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color:
                _indexPage.getPage() == 2 ? ColorsApp.button : ColorsApp.card,
            border: Border.all(
              color: ColorsApp.button,
            ),
          ),
          child: Center(
            child: Text(
              "3",
              style: TextStyle(
                color: _indexPage.getPage() == 2
                    ? ColorsApp.background
                    : ColorsApp.text,
                fontSize: 12,
              ),
            ),
          ),
        ),
      ),
    );
  }

  //   if (!init) {
  //     // build cupertino for category
  //     for (var i = 0; i < category.length; i++) {
  //       cupertinoListCategory.add(
  //         _cupertinoBuilderCategory(category[i]["name"]),
  //       );
  //     }

  //     // build cupertino for brand
  //     for (var i = 0; i < brand.length; i++) {
  //       cupertinoListBrand.add(
  //         _cupertinoBuilderBrand(brand[i]["name"], brand[i]["id"]),
  //       );
  //     }
  //     init = true;
  //   }
  //   _dataProvider = Provider.of<SelectProduct>(context);
  //   _context = context;
  //   return SafeArea(
  //     child: Scaffold(
  //       backgroundColor: ColorsApp.background,
  //       body: Column(
  //         children: [
  //           SizedBox(
  //             height: 10,
  //           ),
  //           _category(),
  //           _divider,
  //           _brand(),
  //           _divider,
  //         ],
  //       ),
  //     ),
  //   );
  // }

  // _category() {
  //   return Container(
  //     child: InkWell(
  //       onTap: () {
  //         _cupertinoCategory();
  //       },
  //       child: Padding(
  //         padding: const EdgeInsets.all(8.0),
  //         child: Row(
  //           textDirection: TextDirection.rtl,
  //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //           mainAxisSize: MainAxisSize.max,
  //           children: [
  //             Text(
  //               StringsApp.category,
  //               style: TextStyle(
  //                 color: ColorsApp.text,
  //               ),
  //             ),
  //             Text(
  //               _dataProvider.getCategory.toString(),
  //               style: TextStyle(
  //                 color: ColorsApp.text,
  //               ),
  //             ),
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }

  // _cupertinoCategory() {
  //   return showCupertinoModalPopup(
  //     context: _context,
  //     builder: (BuildContext context) => CupertinoActionSheet(
  //       title: Text(
  //         StringsApp.selectCategory,
  //         style: TextStyle(
  //           fontFamily: FontsApp.iranSans,
  //           fontWeight: FontWeight.bold,
  //           fontSize: 16.0,
  //           color: Colors.black,
  //         ),
  //       ),
  //       actions: cupertinoListCategory,
  //       cancelButton: CupertinoActionSheetAction(
  //         child: const Text(
  //           'انصراف',
  //           style: TextStyle(
  //             fontFamily: FontsApp.iranSans,
  //             color: Colors.black,
  //             fontSize: 16.0,
  //           ),
  //         ),
  //         isDefaultAction: true,
  //         onPressed: () {},
  //       ),
  //     ),
  //   );
  // }

  // _brand() {
  //   return Container(
  //     child: InkWell(
  //       onTap: () {
  //         _cupertinoBrand();
  //       },
  //       child: Padding(
  //         padding: const EdgeInsets.all(8.0),
  //         child: Row(
  //           textDirection: TextDirection.rtl,
  //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //           mainAxisSize: MainAxisSize.max,
  //           children: [
  //             Text(
  //               StringsApp.brand,
  //               style: TextStyle(
  //                 color: ColorsApp.text,
  //               ),
  //             ),
  //             Text(
  //               brand.firstWhere((element) =>
  //                   element["id"] == _dataProvider.getBrand)["name"],
  //               style: TextStyle(
  //                 color: ColorsApp.text,
  //               ),
  //             ),
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }

  // _cupertinoBrand() {
  //   return showCupertinoModalPopup(
  //     context: _context,
  //     builder: (BuildContext context) => CupertinoActionSheet(
  //       title: Text(
  //         StringsApp.selectBrand,
  //         style: TextStyle(
  //           fontFamily: FontsApp.iranSans,
  //           fontWeight: FontWeight.bold,
  //           fontSize: 16.0,
  //           color: Colors.black,
  //         ),
  //       ),
  //       actions: cupertinoListBrand,
  //       cancelButton: CupertinoActionSheetAction(
  //         child: const Text(
  //           'انصراف',
  //           style: TextStyle(
  //             fontFamily: FontsApp.iranSans,
  //             color: Colors.black,
  //             fontSize: 16.0,
  //           ),
  //         ),
  //         isDefaultAction: true,
  //         onPressed: () {},
  //       ),
  //     ),
  //   );
  // }

  // _cupertinoBuilderCategory(String str) {
  //   return CupertinoActionSheetAction(
  //     child: Text(
  //       str,
  //       style: TextStyle(
  //         fontFamily: FontsApp.iranSans,
  //         fontWeight: FontWeight.bold,
  //         fontSize: 16.0,
  //       ),
  //     ),
  //     onPressed: () {
  //       _dataProvider.setCategory = str;
  //       Navigator.pop(_context);
  //     },
  //   );
  // }

  // _cupertinoBuilderBrand(String str, int id) {
  //   return CupertinoActionSheetAction(
  //     child: Text(
  //       str,
  //       style: TextStyle(
  //         fontFamily: FontsApp.iranSans,
  //         fontWeight: FontWeight.bold,
  //         fontSize: 16.0,
  //       ),
  //     ),
  //     onPressed: () {
  //       _dataProvider.setBrand = id;
  //       Navigator.pop(_context);
  //     },
  //   );

}
