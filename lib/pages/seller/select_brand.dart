import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testApp/data/brand.dart';
import 'package:testApp/providers/page_index.dart';
import 'package:testApp/providers/select_product.dart';
import 'package:testApp/res/colors.dart';
import 'package:testApp/res/strings.dart';

class SelectBrand extends StatelessWidget {
  Divider _divider = Divider(
    color: Colors.grey,
    endIndent: 40,
    indent: 40,
  );
  // List<int> categoryId = [1, 2, 3, 4, 5];
  SelectProduct _brand;
  PageIndex _indexPage;
  @override
  Widget build(BuildContext context) {
    _brand = Provider.of<SelectProduct>(context);
    _indexPage = Provider.of<PageIndex>(context);
    return Column(
      children: [
        Expanded(
          child: ListView.builder(
            itemCount: brand.length,
            itemBuilder: (context, index) {
              return _builder(index);
            },
          ),
        ),
        _acceptbtn(),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }

  _builder(int index) {
    return Column(
      children: [
        InkWell(
          onTap: () {
            List brandId = new List();
            brandId = _brand.getBrand;
            if (_brand.getBrand.contains(index + 1)) {
              brandId.removeWhere((element) => element == index + 1);
              _brand.setBrand = brandId;
            } else {
              brandId.add(index + 1);
              _brand.setBrand = brandId;
            }
          },
          child: Container(
            padding: EdgeInsets.all(10),
            child: Row(
              textDirection: TextDirection.rtl,
              children: [
                Expanded(
                  child: Text(
                    brand[index]["name"],
                    textDirection: TextDirection.rtl,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _brand.getBrand.contains(index + 1)
                          ? ColorsApp.button
                          : ColorsApp.background,
                      border: Border.all(color: ColorsApp.button)),
                  child: Icon(
                    Icons.check,
                    color: ColorsApp.background,
                    size: 18,
                  ),
                )
              ],
            ),
          ),
        ),
        _divider,
      ],
    );
  }

  _acceptbtn() {
    return InkWell(
      onTap: () {
        _indexPage.setPage(2);
      },
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            color: ColorsApp.button),
        width: 150,
        height: 50,
        child: Text(
          StringsApp.accept,
          style: TextStyle(
            color: ColorsApp.background,
          ),
        ),
      ),
    );
  }

}