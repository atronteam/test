import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:testApp/res/colors.dart';
import 'package:testApp/res/strings.dart';

class SelectTakhfif extends StatelessWidget {
  BuildContext _context;
  @override
  Widget build(BuildContext context) {
    _context = context;
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        textDirection: TextDirection.rtl,
        children: [
          _tedadi(),
          SizedBox(
            height: 25,
          ),
          _fixPrice(),
          SizedBox(
            height: 25,
          ),
          _percent(),
          SizedBox(
            height: 40,
          ),
          _acceptbtn()
        ],
      ),
    );
  }

  _tedadi() {
    return Column(
      textDirection: TextDirection.rtl,
      children: [
        Align(
          alignment: Alignment.centerRight,
          child: Text(
            StringsApp.tedadi,
          ),
        ),
        InkWell(
          
          onTap: () {
            cupertino(1);
          },
                  child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 28.0, vertical: 8),
            child: Align(
              alignment: Alignment.centerRight,
              child: Row(
                textDirection: TextDirection.rtl,
                children: [
                  Text(
                    StringsApp.lowCount,
                    textDirection: TextDirection.rtl,
                  ),
                ],
              ),
            ),
          ),
        ),
        InkWell(
          
          onTap: () {
            cupertino(1);
          },
                  child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 28.0, vertical: 8),
            child: Align(
              alignment: Alignment.centerRight,
              child: Row(
                textDirection: TextDirection.rtl,
                children: [
                  Text(
                    StringsApp.countTakhfif,
                    textDirection: TextDirection.rtl,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  _percent() {
    return Column(
      textDirection: TextDirection.rtl,
      children: [
        Align(
          alignment: Alignment.centerRight,
          child: Text(
            StringsApp.percent,
          ),
        ),
        InkWell(
          onTap: () {
            cupertino(1);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 28.0, vertical: 8),
            child: Align(
              alignment: Alignment.centerRight,
              child: Row(
                textDirection: TextDirection.rtl,
                children: [
                  Text(
                    StringsApp.lowPrice,
                    textDirection: TextDirection.rtl,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  _fixPrice() {
    return Column(
      textDirection: TextDirection.rtl,
      children: [
        Align(
          alignment: Alignment.centerRight,
          child: Text(
            StringsApp.price,
          ),
        ),
        InkWell(
          
          onTap: () {
            cupertino(1);
          },
                  child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 28.0, vertical: 8),
            child: Align(
              alignment: Alignment.centerRight,
              child: Row(
                textDirection: TextDirection.rtl,
                children: [
                  Text(
                    StringsApp.lowFactor,
                    textDirection: TextDirection.rtl,
                  ),
                ],
              ),
            ),
          ),
        ),
        InkWell(
          onTap: () {
            cupertino(1);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 28.0, vertical: 8),
            child: Align(
              alignment: Alignment.centerRight,
              child: Row(
                textDirection: TextDirection.rtl,
                children: [
                  Text(
                    StringsApp.priceTakhfif,
                    textDirection: TextDirection.rtl,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  _acceptbtn() {
    return InkWell(
      onTap: () {},
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            color: ColorsApp.button),
        width: 150,
        height: 50,
        child: Text(
          StringsApp.accept,
          style: TextStyle(
            color: ColorsApp.background,
          ),
        ),
      ),
    );
  }

  cupertino(int id) {
    return showCupertinoModalPopup(
      context: _context,
      builder: (BuildContext context) {
        return Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Material(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              alignment: Alignment.center,
              height: 60,
              width: MediaQuery.of(context).size.width,
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: TextField(
                  autofocus: true,
                  keyboardType: TextInputType.number,
                  textDirection: TextDirection.rtl,
                  textAlign: TextAlign.right,
                  decoration: InputDecoration(
                    alignLabelWithHint: true,
                    labelStyle:
                        TextStyle(textBaseline: TextBaseline.alphabetic),
                    border: InputBorder.none,
                    labelText: "مقدار را وارد کنید",
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
