import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:testApp/res/Fonts.dart';
import 'package:testApp/res/colors.dart';

class SingleProduct extends StatefulWidget {
  final id;
  final image;
  final name;
  final price;
  final description;
  final category;
  final count;
  final offer;

  const SingleProduct(
      {Key key,
      this.id,
      this.image,
      this.name,
      this.price,
      this.description,
      this.category,
      this.count,
      this.offer})
      : super(key: key);

  @override
  _SingleProductState createState() => _SingleProductState(
      id: this.id,
      image: this.image,
      name: this.name,
      price: this.price,
      description: this.description,
      category: this.category,
      count: this.count,
      offer: this.offer);
}

class _SingleProductState extends State<SingleProduct> {
  final id;
  final image;
  final name;
  final price;
  final description;
  final category;
  final count;
  final offer;

  _SingleProductState(
      {this.id,
      this.image,
      this.name,
      this.price,
      this.description,
      this.category,
      this.count,
      this.offer});

  bool checkFavorite = false;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Colors.orange, Colors.amberAccent],
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight),
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(30.0),
                    bottomLeft: Radius.circular(30.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12, blurRadius: 2.0, spreadRadius: 2)
                  ]),
              child: Row(
                children: [
                  Expanded(
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: Icon(Icons.home),
                            color: Colors.white)),
                  ),
                  Expanded(
                      child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Text(
                        this.name,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontFamily: FontsApp.iranYekan),
                      ),
                    ),
                  )),
                ],
              ),
            ),
            preferredSize: Size(width, 60)),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: [
            Container(
              width: width,
              height: height / 3,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(this.image), fit: BoxFit.cover),
              ),
            ),
            Container(
              width: width * 0.8,
              height: height / 4,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12, blurRadius: 5.0, spreadRadius: 5)
                ],
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        this.name,
                        style: TextStyle(
                            fontFamily: FontsApp.iranYekan,
                            color: ColorsApp.text,
                            fontWeight: FontWeight.bold),
                      ),
                      Container(
                        width: width / 4,
                        color: Colors.red,
                        height: 2,
                      ),
                      Text(
                        "${this.price.toString() + " تومان "}",
                        style: TextStyle(
                            fontFamily: FontsApp.iranYekan,
                            color: ColorsApp.text,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            if (checkFavorite == false) {
                              checkFavorite = true;
                            } else {
                              checkFavorite = false;
                            }
                          });
                        },
                        child: Icon(
                          checkFavorite == false
                              ? Icons.favorite_border
                              : Icons.favorite,
                          color: Colors.redAccent,
                          size: 50,
                        ),
                      ),
                      Container(
                        width: width / 3,
                        height: 40,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            gradient: LinearGradient(
                                colors: [
                                  Colors.red,
                                  Colors.redAccent,
                                ],
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight)),
                        child: Text(
                          "افزودن به سبد خرید",
                          style: TextStyle(
                              fontFamily: FontsApp.iranYekan,
                              color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.category,
                  size: 60.0,
                  color: Colors.amber,
                ),
                Text(
                  this.category  == 1? "بستنی": this.category == 2? "ماست" : this.category == 3? "دوغ": this.category == 4?"بیسکوئیت":"همه دسته بندی ها",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: FontsApp.iranYekan,
                      color: ColorsApp.text,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                this.description,
                textDirection: TextDirection.rtl,
                style: TextStyle(
                    color: ColorsApp.text, fontFamily: FontsApp.iranYekan),
              ),
            )
          ],
        ),
      ),
    );
  }
}
