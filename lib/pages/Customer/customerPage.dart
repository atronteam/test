import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:testApp/data/data.dart';
import 'package:testApp/res/Assets.dart';
import 'package:testApp/res/Fonts.dart';
import 'package:testApp/res/Widgets.dart';
import 'package:testApp/res/colors.dart';

class CustomerPage extends StatefulWidget {
  @override
  _CustomerPageState createState() => _CustomerPageState();
}

class _CustomerPageState extends State<CustomerPage> {
  List offer1 = new List();
  List offer2 = new List();
  List offer3 = new List();
  List search = new List();
  TextEditingController textSearch;
  int indexSearch = 0;

  Future createLists() async {
    setState(() {
      for (int i = 0; i < fakeData.length; i++) {
        if (fakeData[i]["offer"] == 1) {
          offer1.add(fakeData[i]);
        } else if (fakeData[i]["offer"] == 2) {
          offer2.add(fakeData[i]);
        } else if (fakeData[i]["offer"] == 3) {
          offer3.add(fakeData[i]);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    createLists();

    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size(width, height / 6),
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.orange, Colors.amberAccent],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight),
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(30.0),
                  bottomLeft: Radius.circular(30.0),
                ),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12, blurRadius: 2.0, spreadRadius: 2)
                ]),
            child: Align(
              alignment: Alignment.center,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  InkWell(
                    onTap: () {},
                    child: Container(
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: Icon(
                        Icons.filter_list,
                        color: Colors.amber,
                      ),
                    ),
                  ),
                  Container(
                    width: width * 0.8,
                    height: 40,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(25.0),
                        )),
                    child: TextField(
                      controller: textSearch,
                      textDirection: TextDirection.rtl,
                      onSubmitted: (value) {
                        for (int i = 0; i < fakeData.length; i++) {
                          if (value == fakeData[i]["name"]) {
                            setState(() {
                              search.add(fakeData[i]);
                            });
                          }
                        }
                      },
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintText: "محصول مورد نظر خودتون رو جستجو کنین",
                          contentPadding: EdgeInsets.only(
                              left: 15, bottom: 11, top: 11, right: 15),
                          hintStyle: TextStyle(
                            fontFamily: FontsApp.iranYekan,
                            color: Colors.black12,
                          )),
                    ),
                  ),
                ],
              ),
            ),
          )),
      body: search.length == 0
          ? SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.arrow_back_ios,
                                color: ColorsApp.button,
                              ),
                              Text(
                                "نمایش همه",
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                    color: ColorsApp.button,
                                    fontFamily: FontsApp.iranYekan),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Text(
                              "محصولات تخفیف دار",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                fontFamily: FontsApp.iranYekan,
                                color: ColorsApp.text,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),

                    Container(
                      width: width,
                      height: height / 3,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          reverse: true,
                          itemCount: offer1.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ProductCards(
                              id: offer1[index]["id"],
                              name: offer1[index]["name"],
                              image: offer1[index]["image"],
                              price: offer1[index]["price"],
                              description: offer1[index]["description"],
                              category: offer1[index]["category"],
                              count: offer1[index]["count"],
                              offer: offer1[index]["offer"],
                            );
                          }),
                    ),
                    SizedBox(height: 30),

                    InkWell(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.arrow_back_ios,
                                color: ColorsApp.button,
                              ),
                              Text(
                                "نمایش همه",
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                    color: ColorsApp.button,
                                    fontFamily: FontsApp.iranYekan),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Text(
                              "یکی بخر دوتا ببر...",
                              textDirection: TextDirection.rtl,
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                fontFamily: FontsApp.iranYekan,
                                color: ColorsApp.text,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),

                    Container(
                      width: width,
                      height: height / 3,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: offer2.length,
                          reverse: true,
                          itemBuilder: (BuildContext context, int index) {
                            return ProductCards(
                              id: offer2[index]["id"],
                              name: offer2[index]["name"],
                              image: offer2[index]["image"],
                              price: offer2[index]["price"],
                              description: offer2[index]["description"],
                              category: offer2[index]["category"],
                              count: offer2[index]["count"],
                              offer: offer2[index]["offer"],
                            );
                          }),
                    ),
                    SizedBox(height: 30),

                    InkWell(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.arrow_back_ios,
                                color: ColorsApp.button,
                              ),
                              Text(
                                "نمایش همه",
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                    color: ColorsApp.button,
                                    fontFamily: FontsApp.iranYekan),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Text(
                              "تخفیف های گروهی",
                              textDirection: TextDirection.rtl,
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                fontFamily: FontsApp.iranYekan,
                                color: ColorsApp.text,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),

                    Container(
                      width: width,
                      height: height / 3,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: offer3.length,
                          reverse: true,
                          itemBuilder: (BuildContext context, int index) {
                            return ProductCards(
                              id: offer3[index]["id"],
                              name: offer3[index]["name"],
                              image: offer3[index]["image"],
                              price: offer3[index]["price"],
                              description: offer3[index]["description"],
                              category: offer3[index]["category"],
                              count: offer3[index]["count"],
                              offer: offer3[index]["offer"],
                            );
                          }),
                    ),
                    SizedBox(height: 30),

                  ],
                ),
              ),
            )
          : GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, childAspectRatio: 3 / 2),
              itemCount: search.length,
              itemBuilder: (BuildContext context, int index) {
                return ProductCards(
                  id: search[index]["id"],
                  image: search[index]["image"],
                  price: search[index]["search"],
                  description: search[index]["description"],
                  name: search[index]["name"],
                  category: search[index]["category"],
                  count: search[index]["count"],
                  offer: search[index]["offer"],
                );
              }),
    );
  }
}
