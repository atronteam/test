import 'package:flutter/material.dart';
import 'package:testApp/pages/Customer/customerPage.dart';
import 'package:testApp/pages/seller/sellerPage.dart';
import 'package:testApp/res/Fonts.dart';

class BottomNavigation extends StatefulWidget {
  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {

  TabController tabController;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: 2,
        initialIndex: 1,
        child: Scaffold(
          backgroundColor: Colors.white,
          body: TabBarView(
            controller: tabController,
            children: [
              CustomerPage(),
              SellerPage(),
            ],
          ),
          bottomNavigationBar: bottomNavBar(),
        ),
      ),
    );
  }
}

Widget bottomNavBar() {
  return TabBar(
    indicatorWeight: 1,
    tabs: [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 60.0),
        child: Tab(
          icon: new Icon(Icons.shopping_basket),
          text: "خریدار",
        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 60.0),
        child: Tab(
          icon: new Icon(Icons.person),
          text: "فروشنده",
        ),
      ),
    ],
    labelColor: Colors.blueGrey,
    indicatorColor: Colors.blueGrey,
    unselectedLabelColor: Colors.grey,
    labelStyle: TextStyle(fontSize: 16.0),
    unselectedLabelStyle: TextStyle(
      fontFamily: FontsApp.iranYekan,
      fontSize: 12.0,
    ),
    isScrollable: true,
    indicatorPadding: EdgeInsets.only(bottom: 2.0),
  );
}
